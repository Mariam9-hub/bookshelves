import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { BookFormComponent } from './book-list/book-form/book-form.component';
import { BookListComponent } from './book-list/book-list.component';
import { SingleBookComponent } from './book-list/single-book/single-book.component';
import { AuthGuardsService } from './services/auth-guards.service';

const routes: Routes = [{ path : 'auth/signup' , component : SignupComponent},
{ path : 'auth/signin' , component :SigninComponent },
{ path : 'auth/signup' ,component : SignupComponent},
{ path : 'books', canActivate:[AuthGuardsService]  , component : BookListComponent},
{ path : 'auth/books/new' , canActivate:[AuthGuardsService] , component : BookFormComponent},
{ path : 'auth/books/edit/:id' , canActivate:[AuthGuardsService] , component : BookFormComponent},
{ path : 'auth/view/:id' , canActivate:[AuthGuardsService] , component : SingleBookComponent},
{path : '', redirectTo:'books', pathMatch :'full'},
{path : '**', redirectTo : 'books'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
