import { Component, OnInit , OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import firebase from 'firebase';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{

  isAuth !: any
  constructor(private authService: AuthService,
    private router: Router) { 
     
    }

  ngOnInit() {
    firebase.auth().onAuthStateChanged(user=>{
      if(user) this.isAuth= true
      else this.isAuth= false
    })
  }
  async onSignOut(){
    await this.authService.signOutUser()

    this.router.navigateByUrl('auth/signin')
  }

}
