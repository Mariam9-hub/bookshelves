import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { NgxSpinnerService } from "ngx-spinner";  

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupForm!: FormGroup;
  errorMessage!: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private SpinnerService: NgxSpinnerService) { }

  ngOnInit() : void {
    this.initForm();
  }

  initForm() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }

  async onSubmit() {
    const email = this.signupForm.get('email')?.value ;
    const password = this.signupForm.get('password')?.value;
    this.SpinnerService.show()
    this.authService.createNewUser(email, password).then(async () => {
     
      const userUid = localStorage.getItem("user")
    console.log("userUid",userUid);
    await this.authService.addUser(String(userUid),{email})
        this.router.navigate(['/books']);
        
      },
      (error) => {
        this.errorMessage = error;
        this.SpinnerService.hide()
      }
    );
    this.SpinnerService.hide()
    
   // 
  }
}
