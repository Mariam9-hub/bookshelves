import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'src/app/models/book';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-single-book',
  templateUrl: './single-book.component.html',
  styleUrls: ['./single-book.component.scss']
})
export class SingleBookComponent implements OnInit {
  
  constructor(private bookService : BooksService, private activatedRouter :ActivatedRoute,private router : Router) { }
  book!: Partial<Book>;
  loading: boolean=true
  uid !: string|null
  ngOnInit(): void {
    this.uid=localStorage.getItem("user")
    const snapshotRouter  = this.activatedRouter.snapshot.paramMap.get('id')
    console.log("snapshotRouter",snapshotRouter);
    
    this.bookService.getBookById(snapshotRouter,this.uid).then((book:Book)=>{
      this.book=book
      console.log("book",this.book);
      this.loading=false
    })
    
    
  }

  // onDeleteBook(){
  //   this.bookService.deleteBookById(this.activatedRouter.snapshot.paramMap.get('id'))
  //   this.router.navigate(['books'])
    
  // }
  onEditBook(id :string | undefined){
    this.router.navigate(['/auth','books','edit',id])

  }
  onBack(){
    this.router.navigate(['/auth','books'])
  }

}
