import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../models/book';
import { BooksService } from '../services/books.service';
import { NgxSpinnerService } from "ngx-spinner";  
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {

  constructor( private bookService : BooksService, private router:Router,
    private SpinnerService: NgxSpinnerService, private authService : AuthService) { }
  books : Book[] =[];
  loading: boolean=true
  uid !: string | null
  ngOnInit(): void { 
 this.uid  = localStorage.getItem("user")
    this.bookService.getBooks(this.uid ).subscribe(data=>{
      console.log("data",data);
      
      this.books = data
     
      console.log("books",this.books);
      this.loading =false 
    })
    
  }
  onAddBook(){
this.router.navigateByUrl("auth/books/new")
  }
  /**
   * delete book
   * @param book
   */

  onDeleteBook(book : Book){
    this.bookService.deleteBookById(book,this.uid )
    this.router.navigateByUrl("books")
  }
  /**
   * View book details
   * @param book
   */
  onViewBook(book :Book){
    this.router.navigate(['/auth','view',book.id])
  }
}
