import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'src/app/models/book';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {

  constructor( private formBuilder : FormBuilder, private bookService : BooksService, private router : Router, private activatedRoute : ActivatedRoute) { }
 
  bookform!: FormGroup;
  loading : boolean=true
  isEdit : boolean=false
  editBook!: string | null;
  buttonName: string ="Ajouter un livre"
  fileIsUploading: boolean= false;
  fileUploaded : boolean= false ;
  fileUrl !: string
  uid !:string|null
  book!:Partial<Book>;
  ngOnInit(): void {
    this.uid = localStorage.getItem("user")
    this.router.url.includes("auth/books/new") ? this.loading=false : null
    this.initForm()
    this.editBook = this.activatedRoute.snapshot.paramMap.get('id')
    if (this.editBook) {
      this.buttonName="Modifier"
      this.bookService.getBookById(this.editBook, this.uid).then((book:Book)=>{
        this.bookform.get("title")?.setValue(book.title)
        this.bookform.get("author")?.setValue(book.author)
        this.loading=false
      })
      
    }
  }
  initForm() {
    this.bookform= this.formBuilder.group({
      title : ['',[Validators.required]],
      author : ['',[Validators.required]]
    })
  }

  addNewBook(){
    console.log(this.bookform.value);
    this.book={title:this.bookform.get("title")?.value,
  author:this.bookform.get("author")?.value }
    if(this.fileUrl && this.fileUrl!==""){
      this.book={...this.book,photo:this.fileUrl}
      console.log("book",this.book);
      
    }
    if(this.editBook){
      this.bookService.editBook(this.editBook,this.book,this.uid)
      this.router.navigateByUrl('books')
    }
    else{
    this.bookService.addBook(this.book, this.uid);
    this.router.navigateByUrl('books')}
  }
  onUploadFile(file: File){
     this.fileIsUploading= true
    this.bookService.uploadFile(file).then((url : string)=>{
      //console.log("url",url);
      
      this.fileUrl=url
      this.fileUploaded=true
      this.fileIsUploading=false;

    })

  }
  saveImage(event :any){
    console.log("event",event.target.files[0]);
    
    this.onUploadFile(event.target.files[0])

  }
}
