export interface Book{
    photo: string;
    title : string;
    author : string;
    id : string;
}