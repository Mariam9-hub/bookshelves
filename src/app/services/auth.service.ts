import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase';
import { Observable, Unsubscribable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afs : AngularFirestore,
    private afAuth : AngularFireAuth) {
    
     }
  
  createNewUser(email : string, password : string):Promise<void> {
   
   return new Promise<void>((resolve, reject)=>{
    firebase.auth().createUserWithEmailAndPassword(email,password).then(async ()=>{
      resolve();
     await this.currentUser()
      
    },(err)=>{
      reject(err)
    })
   })
  }

  signInUser(email : string, password: string):Promise<void>{

    return new Promise<void>((resolve,reject)=>{
      firebase.auth().signInWithEmailAndPassword(email,password).then(async ()=>{
        resolve()
      await this.currentUser()
        },(err)=>{
        reject(err);
      })
      })
    }
  signOutUser() : Promise<void>{
    localStorage.clear()

   return firebase.auth().signOut();
    
  }
  /***
   * Set current user uid in local storage
   */
  async currentUser() {
    const uid :any= await this.afAuth.currentUser.then(user=>{
      user ?
      localStorage.setItem('user',String(user?.uid)) : null
    }) .catch(error=>{
      console.log("Error : "+ error);
      
    })   
  }

/**
 * add user to firestore
 * @param uid 
 * @param user
 */
  async addUser( uid : string,user : any) : Promise<void>{
  
  return this.afs.collection("users").doc(uid).set(user)
}

}
