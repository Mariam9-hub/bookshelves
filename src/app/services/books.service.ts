import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import firebase from 'firebase';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Book } from '../models/book';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { FIREBASE_OPTIONS } from '@angular/fire';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(
    private afs : AngularFirestore,
    private fbStorage : AngularFireStorage,
    private authservice : AuthService
  ) {
   
     
      
   }


   /** Add new book
    * @param newBook
    * @returns {void}
    */
  addBook(book:Partial<Book>, uid: string | null): void{
    this.afs.collection(`users/${uid}/books`).add(book)

  }

  /**
   * Get all books from firebase odered by created date   
   * @returns {books}
   */
  getBooks(uid: string | null): Observable<Book[]>{
    return this.afs.collection<any>(`users/${uid}/books`)
    .snapshotChanges()
    .pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as any;
          const id = a.payload.doc.id;
          const path = a.payload.doc.ref.path;
          return { id, path, ...data };
        });
      }),
    );

  }

  /**
   * Get book by Id
   * @param id
   * @returns {book}
   * 
   */
  getBookById(id:string  | null, uid: string | null):Promise<Book>{
    return this.afs.doc(`users/${uid}/books/${id}`).ref.get().then(doc => {
      const data = doc.data() as any;
      const id = doc.id;
      const path = doc.ref.path;
      return { id, path, ...data };
    });

  }

  /**
   * Edit book()
   * @param id
   * @param data
   * @returns {void}
   */
  editBook(id: string,data :any, uid: string| null):Promise<void|Book>{
    return this.afs.doc(`users/${uid}/books/${id}`).update(data)
  }

/**
 * Delete book 
 * @param id
 *@returns {void}
 */
  deleteBookById(book : Book | null, uid : string| null):Promise<void>{
    if (book?.photo) {
      const storageRef = this.fbStorage.storage.refFromURL(book.photo)
      storageRef.delete().then(()=>{
        console.log('photo suprimée !');
        
      },(error)=>{
        console.log("photo non truvée :" + error);
        
      })
      
    }
    return  this.afs.doc(`users/${uid}/books/${book?.id}`).delete()

  }
  /**
   * Upload Image
   * @param file
   */
  uploadFile(file : File) : Promise<string>{
    return new Promise((resolve,reject)=>{
      const fileName=Date.now()
      const upload = this.fbStorage.storage.ref().child('/image'+fileName+file.name).put(file)
      upload.on(firebase.storage.TaskEvent.STATE_CHANGED,()=>{
        console.log("chargement ...")},
        (error)=>{
          console.log('Erreur de chargement :'+ error);
          reject()
        },
        ()=>{
          resolve(upload.snapshot.ref.getDownloadURL())
        }
      )
    })

  }
}
