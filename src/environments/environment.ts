// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase :{
    apiKey: "AIzaSyArOtEluFnBuxIXMamOLeHl_D8g0fLQ-Zs",
    authDomain: "bookshelves-26973.firebaseapp.com",
    projectId: "bookshelves-26973",
    storageBucket: "bookshelves-26973.appspot.com",
    messagingSenderId: "84590786214",
    appId: "1:84590786214:web:6dfc01d6adb241894ba667",
    measurementId: "G-41PMW9ZZ4N"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
